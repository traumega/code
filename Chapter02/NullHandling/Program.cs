﻿#nullable enable
using System;


namespace NullHandling
{
    class Address
    {
        public string? Building;
        public string? Street = string.Empty;
        public string City = string.Empty;
        public string? Region = string.Empty;

    }
    class Program
    {
        static void Main(string[] args)
        {
            // int thisCantBeNull = 4;
            
            // int? thisCouldAlsoBeNull = null;

            // System.Console.WriteLine(thisCantBeNull);
            // System.Console.WriteLine(thisCouldAlsoBeNull.GetValueOrDefault());
            // thisCouldAlsoBeNull = 7;
            // System.Console.WriteLine(thisCouldAlsoBeNull);

            var address = new Address();
            address.Building = null;
            address.Street = null;
            address.City = "London";
            address.Region = null;

            string authorName = null;
           
            int? y = authorName?.Length;
            //assign value if value is null
            var result = authorName?.Length ?? 3;
            System.Console.WriteLine(result);

            

           
        }
    }
}
