﻿using System;
using static System.Console;

namespace Ternary
{
    class Program
    {
        static void Main()
        {
            string incomingName = "phill";
            object red = ConsoleColor.Red;
            object green = ConsoleColor.Green;
            string username = incomingName.Length > 6 ? incomingName : "default";
            
            ConsoleColor color = incomingName.Length > 6 ? ConsoleColor.Green : ConsoleColor.Red;
            Console.ForegroundColor = color;

            System.Console.WriteLine("what color am i");
            Console.ResetColor();

            

        }
    }
}
