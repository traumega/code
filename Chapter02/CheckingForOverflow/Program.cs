﻿using System;
using static System.Console;

namespace CheckingForOverFlow
{
    class Program
    {
        static void Main(string[] args)
        {
        //    try
        //    {
        //        checked
        //        {
        //             int x = int.MaxValue -1;
        //             System.Console.WriteLine($"intial value: {x}");
        //             x++;
        //             System.Console.WriteLine($"after incrementing : {x}");
        //             x++;
        //             System.Console.WriteLine($"after incrementing : {x}");
        //             x++;
        //             System.Console.WriteLine($"after incrementing : {x}");
        //        }
                
        //    }
        //    catch(OverflowException)
        //    {
        //        System.Console.WriteLine("the number has casue the code to overflow but i caught the exception");
        //    }

        //    unchecked
        //    {
        //        int y = int.MaxValue + 1;
        //        System.Console.WriteLine($"initial value of y is {y}");
        //        y--;
        //        System.Console.WriteLine($"after decrementing y is now {y}");
        //        y--;
        //        System.Console.WriteLine($"after decrementing y is now {y}");
        //        y--;
        //        System.Console.WriteLine($"after decrementing y is now {y}");
        //    }

        double one = 5;
        double answer = one / 0;
        System.Console.WriteLine($"the answer is {answer}");
           
           
        }
    }
}
