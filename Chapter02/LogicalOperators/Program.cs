﻿using System;
using static System.Console;
using System.IO;

namespace LogicalOperators
{
    class Program
    {
        static void Main()
        {
            int number = 10;
            bool lessThan = number < 20;
            bool moreThan = number > 5;
            bool both = number > 9 && number < 12;
            bool bothVar = lessThan && moreThan;

            // System.Console.WriteLine(lessThan);
            // System.Console.WriteLine(moreThan);
            // System.Console.WriteLine(both);
            // System.Console.WriteLine(bothVar);
            System.Console.WriteLine("enter a name");
            string name = Console.ReadLine();
           
            bool firstBool = ((name.Length >= 5 && name.Length <= 14) && ((int)name[0] >= 97 && (int)name[0] <= 122) && (name != "admin"));
            bool secondBool = (name.Length >= 6 && name.Length <= 14) && !((int)name[0] >= 48 && (int)name[0] <=57);

           
            bool bothTest = firstBool || secondBool;
            if(bothTest)
            {
                System.Console.WriteLine("the name passed");
            }
            else
            {
                System.Console.WriteLine("the name did not pass and might be admin");
            }
            
           

        }
    }
}
