﻿using System;
using static System.Console;
using static System.Convert;

namespace Iterator
{
    class Program
    {
        static void Main(string[] args)
        {
            // string[] names  = {"Adam", "Barry", "Charlie" };
            // foreach(string name in names)
            // {
            //     System.Console.WriteLine($"{name} has {name.Length} characters.");
            // }


             System.Console.WriteLine("Before Parsing");
             Write("what is your age ");
             string input = ReadLine();
             try
             {
                 int age = int.Parse(input);
                 System.Console.WriteLine($"your age is {age}.");
             }
             catch(OverflowException)
             {
                 System.Console.WriteLine("your age is a valid number format but its is either too big or small");
             }
             catch(FormatException)
             {
                 System.Console.WriteLine("the age you entered is not a valid number format.");
             }
             catch(Exception ex)
             {
                 System.Console.WriteLine($"{ex.GetType()} says {ex.Message}");
             }
            
             System.Console.WriteLine("after catch");

        }
    }
}
